from app import db
from flask_login import UserMixin
class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
class Menulist(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    Dish = db.Column(db.String(50))
    Price = db.Column(db.Integer)
    order = db.relationship('Order',backref='dish', lazy=True)
    def __init__(self,Dish,Price):
        self.Dish=Dish
        self.Price=Price
class Table(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    Table_Name = db.Column(db.String(50))
    Status = db.Column(db.String(50))
    order = db.relationship('Bill',backref='table_detail', lazy=True)
    def __init__(self,Table_Name,Status):
        self.Table_Name=Table_Name
        self.Status=Status
sub = db.Table('sub',
    db.Column('order_id', db.Integer, db.ForeignKey('order.id')),
    db.Column('bill_id', db.Integer, db.ForeignKey('bill.id'))
)
class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Dish=db.Column(db.Integer,db.ForeignKey('menulist.id'))
    Quantity=db.Column(db.Integer())
    order=db.relationship('Bill',secondary='sub',backref=db.backref('order_detail',lazy='dynamic'))
class Bill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Table_Name=db.Column(db.Integer,db.ForeignKey('table.id'))
    Amount=db.Column(db.Integer)
