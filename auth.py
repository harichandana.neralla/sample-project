from flask import Blueprint,render_template,request,redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user,logout_user,login_required
from app import db
from models import User
auth = Blueprint('auth', __name__)

@auth.route('/login',methods=['POST','GET'])
def login():
    if request.method=='POST':
        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False
        user = User.query.filter_by(email=email).first()
        if not user or not check_password_hash(user.password, password):
            return redirect(url_for('auth.login'))
        login_user(user, remember=remember)
        return redirect(url_for('views.dashboard'))
    return render_template('login.html')

@auth.route('/signup',methods=['POST','GET'])
def signup():
    if request.method=='POST':
        email = request.form.get('Email')
        name = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(email=email).first() 
        if user:
            return redirect(url_for('auth.signup'))
        new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))
        db.session.add(new_user)
        db.session.commit()
        print(new_user)
        return redirect(url_for('auth.login'))
    return render_template('signup.html')
@login_required
@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('views.index'))
    