from app import app,db
from models import *
from views import bp
from auth import auth
from flask_login import LoginManager
app.register_blueprint(bp)
app.register_blueprint(auth)
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)



@login_manager.user_loader
def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.query.get(int(user_id))
if __name__=='__main__':
    app.run(debug=True)