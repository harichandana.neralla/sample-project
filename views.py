from flask import Blueprint,render_template,request,session,url_for,redirect
from flask_login import login_required
from app import db
from models import *
bp=Blueprint("views",__name__,template_folder="templates")

@bp.route("/")
def index():
    return render_template('index.html')

@bp.route("/dashboard")
@login_required
def dashboard():
    return render_template('dashboard.html')
@bp.route("/menu")
@login_required
def Menu():
    return render_template('Menu_list.html',menu=Menulist.query.all())
@bp.route("/addDish",methods=['GET','POST'])
@login_required
def addDish():
    Dish=request.form['Dish']
    Price=request.form['Price']
    menu=Menulist(Dish=Dish,Price=Price)
    db.session.add(menu)
    db.session.commit()
    return redirect(url_for('views.Menu'))
@bp.route("/table",methods=['Get'])
@login_required
def Table_List():
    return render_template('Tables_list.html',table=Table.query.all())
@bp.route("/addTable",methods=['GET','POST'])
@login_required
def addTable():
    Table_Name=request.form['Table_Name']
    Status=request.form['Status']
    table=Table(Table_Name=Table_Name,Status=Status)
    db.session.add(table)
    db.session.commit()
    return redirect(url_for('views.Table_List'))
@bp.route("/update_Table_Status/<int:pk>",methods=['POST'])
@login_required
def update_Table_Status(pk):
    table=Table.query.filter_by(id=pk).all()[0]
    print(table)
    if table.Status=="Available":
        table.Status="Booked"
    else:
        table.Status="Available"
    db.session.commit()
    return redirect(url_for('views.Table_List'))
@bp.route("/Bills")
@login_required
def Bills():
    return render_template('CustomerBill_list.html',Bills=Bill.query.all())

@bp.route("/newbill")
@login_required
def NewBill():
    print(Menulist.query.all())
    return render_template('NewBill.html',Table_list=Table.query.all(),Menu_list=Menulist.query.all())
@bp.route("/add_order_to_bill",methods=['POST'])
@login_required
def Add_Order_To_Bill():
    Table_Name=request.form['Table_Name']
    Dish=request.form['Dish']
    Quantity=request.form['Quantity']
    dish=Menulist.query.filter_by(Dish=Dish)[0]
    order=Order(dish=dish,Quantity=Quantity)
    db.session.add(order)
    db.session.commit()
    table=Table.query.filter_by(Table_Name=Table_Name)[0]
    bill=Bill(table_detail=table,Amount=0)
    db.session.add(bill)
    db.session.commit()
    bill.order_detail.append(order)
    db.session.commit()
    amount=0
    for item in bill.order_detail.all():
        amount=amount+item.dish.Price*item.Quantity
    bill.Amount=amount
    db.session.commit()
    return render_template('Orders_To_Bill.html',Bill_Orders_Objs=bill.order_detail.all(),Bill_Obj=bill,Menu_list=Menulist.query.all())
@bp.route('/multiple_orders_to_bill/<int:pk>',methods=['POST'])
@login_required
def Multiple_Orders_to_Bill(pk):
    Dish=request.form['Dish']
    Quantity=request.form['Quantity']
    dish=Menulist.query.filter_by(Dish=Dish)[0]
    order=Order(dish=dish,Quantity=Quantity)
    db.session.add(order)
    db.session.commit()
    bill=Bill.query.filter_by(id=pk)[0]
    bill.order_detail.append(order)
    db.session.commit()
    amount=0
    for item in bill.order_detail.all():
        amount=amount+item.dish.Price*item.Quantity
    bill.Amount=amount
    db.session.commit()
    return render_template('Orders_To_Bill.html',Bill_Orders_Objs=bill.order_detail.all(),Bill_Obj=bill,Menu_list=Menulist.query.all())
@bp.route("/Bill_Details/<int:pk>")
@login_required
def Bill_Details(pk):
    bill=Bill.query.filter_by(id=pk)[0]
    table=Table.query.filter_by(Table_Name=bill.table_detail.Table_Name)[0]
    table.Status="Available"
    db.session.commit()
    return render_template('Final_Bill.html',Bill_Orders_Objs=bill.order_detail.all(),Bill_Obj=bill)